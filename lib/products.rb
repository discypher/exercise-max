# frozen_string_literal: true

require_relative 'product.rb'

# Store the list of available products
class Products
  attr_reader :products

  PRODUCTS = [
    { name: 'milk', base_price: 397, sale_price: 500, sale_quantity: 2 },
    { name: 'bread', base_price: 217, sale_price: 600, sale_quantity: 3 },
    { name: 'banana', base_price: 99 },
    { name: 'apple', base_price: 89 }
  ].freeze

  def initialize
    @products = []
    PRODUCTS.each do |product|
      @products << Product.new(product)
    end
  end
end
