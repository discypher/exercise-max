# Handle printing and formatting results
class Receipt
  GRID_WIDTH = 10
  # Pass in a Totals object
  def initialize(totals, selected_products)
    @totals = totals
    @selected_products = selected_products
  end

  def to_s
    result = "Item      Quantity  Price     \n"
    result << "#{'-' * 30}\n"
    result << build_product_lines
    result << "\n"
    result << "Total price : $#{format_price(@totals.total_price)}\n"
    result << "Total savings: $#{format_price(@totals.total_savings)}"
  end

  private

  def build_product_lines
    result = ''
    @selected_products.each do |p|
      result << format_spacing(p.name)
      result << format_spacing(p.count)
      result << format_spacing("$#{format_price(p.price)}")
      result << "\n"
    end
    result
  end

  def format_spacing(text)
    text = text.to_s
    spaces = GRID_WIDTH - text.length
    "#{text}#{' ' * spaces}"
  end

  def format_price(price)
    '%0.2f' % (price / 100.0)
  end
end
