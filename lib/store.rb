# frozen_string_literal: true

require_relative 'cart.rb'
require_relative 'products.rb'
require_relative 'register.rb'

# A store to hold other objects
class Store
  attr_accessor :register,
                :products

  def initialize
    @products = Products.new
    @register = Register.new
  end

  def grab_a_cart
    Cart.new
  end

  def checkout(cart)
    @register.checkout(cart)
  end
end
