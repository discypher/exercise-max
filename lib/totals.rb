# frozen_string_literal: true

# Calculate aggregate totals for all selected products
class Totals
  attr_accessor :total_price,
                :total_savings,
                :selected_products
  def initialize(selected_products)
    @selected_products = selected_products
    @total_price = 0.0
    @total_savings = 0.0
    calculate_totals
  end

  private

  def calculate_totals
    @selected_products.each do |p|
      @total_price += p.price
      @total_savings += p.savings
    end
  end
end

