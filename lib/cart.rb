# frozen_string_literal: true

require_relative 'selected_product.rb'
require_relative 'products.rb'
require_relative 'product.rb'

# Somewhere to put our selected items
# input:
class Cart
  attr_accessor :selected_products

  def initialize
    @products = Products.new
    @selected_products = []
  end

  def add_items(items)
    items.each do |item|
      next unless product_exists?(item)

      product = grab_product(item)
      if product_already_selected?(product)
        @selected_products.select { |p| p.name == product.name }.first.add_one
      else
        @selected_products << SelectedProduct.new(product)
      end
    end
  end

  private

  def grab_product(item_name)
    @products.products.select { |p| p.name == item_name }.first
  end

  def product_already_selected?(product)
    @selected_products.map { |p| p.name == product.name }.any?
  end

  def product_exists?(item)
    @products.products.map { |product| product.name == item.downcase }.any?
  end
end
