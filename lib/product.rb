# frozen_string_literal: true

# Represent individual available products
class Product
  attr_accessor :name,
                :price,
                :sale_price,
                :sale_quantity

  def initialize(product)
    @name = product[:name]
    @price = product[:base_price]
    @sale_price = product[:sale_price] if product[:sale_price]
    @sale_quantity = product[:sale_quantity] if product[:sale_quantity]
  end
end
