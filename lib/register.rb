# frozen_string_literal: true

require_relative 'totals.rb'
require_relative 'receipt.rb'

# Handle checkout and receipts
class Register
  def checkout(cart)
    totals = Totals.new(cart.selected_products)
    receipt = Receipt.new(totals, cart.selected_products)
    puts receipt.to_s
  end
end
