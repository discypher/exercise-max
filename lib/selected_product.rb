# frozen_string_literal: true

require_relative 'product.rb'

# Perform calculations per product purchased
class SelectedProduct
  attr_accessor :count,
                :price,
                :savings,
                :product

  def initialize(product)
    @product = product
    @count = 1
    @price = product.price
    @savings = 0.0
  end

  def name
    @name ||= @product.name
  end

  def add_one
    @count += 1
    calculate_price
    calculate_savings if on_sale?
  end

  private

  def calculate_price
    @price = if on_sale?
               aggregate_sale_price
             else
               aggregate_price
             end
  end

  def aggregate_sale_price
    item_price = 0.0
    item_price += @count / @product.sale_quantity * @product.sale_price
    item_price += @count % @product.sale_quantity * @product.price
    item_price
  end

  def aggregate_price
    @count * product.price
  end

  def calculate_savings
    @savings = aggregate_price - aggregate_sale_price
  end

  def on_sale?
    !@product.sale_price.nil?
  end
end
