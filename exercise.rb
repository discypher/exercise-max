# frozen_string_literal: true

require_relative 'lib/store.rb'

def item_list
  p 'Please enter all the items purchased separated by a comma'
  gets.chomp.split(',').map(&:strip).map(&:downcase)
end

if __FILE__ == $0
  store = Store.new
  items = item_list
  cart = store.grab_a_cart
  cart.add_items(items)
  store.checkout(cart)
end
